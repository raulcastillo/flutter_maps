import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geodesy/geodesy.dart' as geo;

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // ignore: avoid_init_to_null
  Position? _currentPosition;
  bool bandera = false;
  Set<Marker> _markers = Set();
  Set<Polyline> _polyline = {};
  List<Polyline> poligono = [];
  geo.Geodesy geodesy = geo.Geodesy();
  Completer<GoogleMapController> _controller = Completer();
  List<LatLng> latlngSegment1 = [
    LatLng(19.3959295, -99.1828104),
    LatLng(19.365162, -99.1793771),
    LatLng(19.3509098, -99.1584345),
    LatLng(19.3535012, -99.1282221),
    LatLng(19.3810323, -99.1110559),
    LatLng(19.3981964, -99.096293),
    LatLng(19.394958, -99.1347452),
    LatLng(19.4033776, -99.1368051),
    LatLng(19.4033776, -99.1254755),
    LatLng(19.4111492, -99.1244455),
    LatLng(19.4079111, -99.1086527),
    LatLng(19.4075873, -99.098353),
    LatLng(19.4169776, -99.0980097),
    LatLng(19.4208631, -99.0942331),
    LatLng(19.4276625, -99.1100259),
    LatLng(19.4292814, -99.1306253),
    LatLng(19.4079111, -99.1340585),
    LatLng(19.4056444, -99.1447015),
    LatLng(19.3884811, -99.1429849),
    LatLng(19.381033, -99.1285654),
    LatLng(19.3739071, -99.1405817),
    LatLng(19.3800607, -99.1567178),
    LatLng(19.4069396, -99.1567178),
    LatLng(19.4208631, -99.152598),
    LatLng(19.4231296, -99.161181),
    LatLng(19.4134158, -99.1814371),
    LatLng(19.3959295, -99.1828104),
  ];
  List<LatLng> latlngSegment2 = [
    LatLng(19.4638269, -99.1638816),
    LatLng(19.4230352, -99.1793311),
    LatLng(19.4524969, -99.1721213),
    LatLng(19.4557341, -99.2198432),
    LatLng(19.4100834, -99.2414725),
    LatLng(19.3916252, -99.2438758),
    LatLng(19.3741365, -99.2301429),
    LatLng(19.3592373, -99.21641),
    LatLng(19.3864435, -99.1958106),
    LatLng(19.3663632, -99.1927207),
    LatLng(19.3530829, -99.2119468),
    LatLng(19.3446606, -99.2043937),
    LatLng(19.3326744, -99.193064),
    LatLng(19.3090234, -99.1889441),
    LatLng(19.2983307, -99.1539252),
    LatLng(19.3099954, -99.139849),
    LatLng(19.3216593, -99.0880072),
    LatLng(19.3466043, -99.095217),
    LatLng(19.3585895, -99.0608847),
    LatLng(19.3900059, -99.0608847),
    LatLng(19.4084643, -99.0567649),
    LatLng(19.4330722, -99.0519584),
    LatLng(19.4887501, -99.0231192),
    LatLng(19.5418204, -99.0135062),
    LatLng(19.570937, -99.0238059),
    LatLng(19.5482912, -99.0588248),
    LatLng(19.5521736, -99.0794242),
    LatLng(19.4816295, -99.0993369),
    LatLng(19.4842189, -99.1089499),
    LatLng(19.4991066, -99.1055167),
    LatLng(19.5230536, -99.1027701),
    LatLng(19.539232, -99.1206229),
    LatLng(19.5379378, -99.1412223),
    LatLng(19.5508795, -99.1968406),
    LatLng(19.513993, -99.2373526),
    LatLng(19.4602661, -99.2421592),
    LatLng(19.4874554, -99.2112601),
    LatLng(19.4783928, -99.1851676),
    LatLng(19.4638269, -99.1638816),
  ];

  var posicionInicial;
  LatLng posicionAnterior = LatLng(0, 0);
  LatLng PosicionNueva = LatLng(0, 0);

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  _getCurrentLocation() async {
    await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        posicionInicial = CameraPosition(
            target:
                LatLng(_currentPosition!.latitude, _currentPosition!.longitude),
            zoom: 15);
        posicionAnterior =
            LatLng(_currentPosition!.latitude, _currentPosition!.longitude);
        PosicionNueva =
            LatLng(_currentPosition!.latitude, _currentPosition!.longitude);
      });
    }).catchError((e) {
      print(e);
    });
    bandera = true;
    Marker f = Marker(
        markerId: MarkerId('1'),
        position:
            LatLng(_currentPosition!.latitude, _currentPosition!.longitude));
    _markers.add(f);
  }

  cordenadaXpunto(LatLng n) {
    //List<LatLng> listai;

    geo.LatLng ubicacion = geo.LatLng(n.latitude, n.longitude);

    for (var i = 0; i < poligono.length; i++) {
      var resul = listaRecorrido(poligono[i], ubicacion);
      if (resul != null) {
        return resul;
      }
    }
    return 'Fuera de zona';
  }

  listaRecorrido(Polyline poligon, geo.LatLng n) {
    List<geo.LatLng> nuevoRecorrido = [];
    var v = poligon.points;
    for (var i = 0; i < v.length; i++) {
      nuevoRecorrido.add(geo.LatLng(v[i].latitude, v[i].longitude));
    }
    bool isGeoPointInPolygon = geodesy.isGeoPointInPolygon(n, nuevoRecorrido);
    if (isGeoPointInPolygon == true) {
      return poligon.polylineId.value;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Seleccione su ubicacion deseada'),
            shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(15),
      ),
    ),
      ),
      body: bandera == true
          ? GoogleMap(
              initialCameraPosition: posicionInicial,
              myLocationButtonEnabled: true,
              compassEnabled: true,
              zoomControlsEnabled: false,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);

                setState(() {
                  _polyline.add(Polyline(
                    polylineId: PolylineId('area 2'),
                    visible: true,
                    //latlng is List<LatLng>
                    points: latlngSegment1,
                    width: 2,
                    color: Colors.blue,
                  ));
                  poligono.add(Polyline(
                    polylineId: PolylineId('area 2'),
                    visible: true,
                    //latlng is List<LatLng>
                    points: latlngSegment1,
                    width: 2,
                    color: Colors.blue,
                  ));
                  _polyline.add(Polyline(
                    polylineId: PolylineId('area 1'),
                    visible: true,
                    //latlng is List<LatLng>
                    points: latlngSegment2,
                    width: 2,
                    color: Colors.blue,
                  ));
                  poligono.add(Polyline(
                    polylineId: PolylineId('area 1'),
                    visible: true,
                    //latlng is List<LatLng>
                    points: latlngSegment2,
                    width: 2,
                    color: Colors.blue,
                  ));
                });
              },
              markers: _markers,
              polylines: _polyline,
              onTap: (position) async {
                PosicionNueva = position;
                String variable = cordenadaXpunto(position);
                Fluttertoast.showToast(
                    msg: "$variable",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);

                Marker f = Marker(markerId: MarkerId('1'), position: position);
                setState(() {
                  _markers.add(f);
                });
                final GoogleMapController controller = await _controller.future;
                var mover = CameraPosition(
                    target: LatLng(position.latitude, position.longitude),
                    zoom: 15);
                controller.animateCamera(CameraUpdate.newCameraPosition(mover));
              },
            )
          : Container(
              child: Center(child: CircularProgressIndicator()),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Marker f =
              Marker(markerId: MarkerId('1'), position: posicionAnterior);
          setState(() {
            _markers.add(f);
          });
          final GoogleMapController controller = await _controller.future;
          var mover = CameraPosition(target: posicionAnterior, zoom: 15);
          controller.animateCamera(CameraUpdate.newCameraPosition(mover));
        },
        child: Icon(Icons.keyboard_return),
        backgroundColor: Colors.green,
      ),
    );
  }
}
